*** Settings ***

Library     AppiumLibrary

Resource    ../../config/android/elements.robot


*** Keywords ***

open the app
    Run Keyword If    'android' in ${TEST_TAGS}    Open Android App
    ...    ELSE IF    'ios' in ${TEST_TAGS}    Open IOS App
    ...    ELSE    Log    No specific platform found in tags


Open Android App
    Log    [DEBUG] Opening Android App 
    Open Application    http://127.0.0.1:4725/wd/hub
    ...                 automationName=UiAutomator2
    ...                 platformName=Android
    ...                 deviceName=sdk_gphone64_arm64
    ...                 app=${EXECDIR}/app/Reflectly_Mood_Tracker_Diary_4_9_0_Apkpure.apk
    ...                 udid=emulator-5554

    ${orig timeout} =	Set Appium Timeout	15 seconds
    Log    [DEBUG] Android App opened successfully

Open IOS App
    #Open Application    
    #...                 automationName=XCUITest
    #...                 platformName=IOS
    #...                 deviceName=iPhone 15 Pro Max
    #...                 app= ${EXECDIR}/app/vNu-Virtual-Phone-Number.ipa
    #...                 udid=9CC8646B-0BA6-4065-9165-B6A9F2D9BF70
    #...                 bundleId=com.wecan.vNu

    ${orig timeout} =	Set Appium Timeout	15 seconds
    
the user validate the app openned
    Wait Until Page Contains Element   ${SLOGAN_APP}

click create account
    Wait Until Element Is Visible   ${CREATE_ACCOUNT}
    Click Element       ${CREATE_ACCOUNT}

validates that start the register page
    Wait Until Element Is Visible   ${YOUR_NICK_NAME}

Close_App
    Run Keyword If    Application Should Be Open    Capture Page Screenshot
    Close Application